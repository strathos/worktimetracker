'use strict';
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Promise = require('bluebird');
const config = require('./config');

// DB
mongoose.connect(config.mongodbUri, { useMongoClient: true });
mongoose.Promise = Promise;

// Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Routes
app.use('/alive', require('./routes/alive'));
const v1 = require('./routes/v1');
app.use('/api/v1/', v1);
app.use('/api/', v1);

// Server
app.listen(config.port, () => {
  console.log("REST API server listening on port: " + config.port);
  let db = mongoose.connection;
  db.on('error', console.error.bind(console, 'MongoDB connection error:'));
  db.once('open', () => {
    console.log("Connected to MongoDB server: " + config.mongodbUri);
  });
});
