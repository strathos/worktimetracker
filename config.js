'use strict';
const mongodbHostname = process.env.MONGODB_HOSTNAME || 'localhost';
const mongodbDatabase = 'worktimetracker';

module.exports = {
  // Server config
  port: process.env.PORT || 3000,

  // MongoDB config
  mongodbUri: 'mongodb://' + mongodbHostname + '/' + mongodbDatabase
};
