'use strict';
const router = require('express').Router();
const AppUser = require('../../models/appuser');

router.get('/list', (req, res) => {
  AppUser.find({}).then((appusers) => {
    res.status(200).send(appusers);
  }).catch( err => {
    res.status(400).send('error: ' + err);
  });
});

router.post('/register', (req, res) => {
  AppUser.create(req.body).then((appuser) => {
    res.status(200).send(appuser);
  }).catch( err => {
    res.status(400).send('error: ' + err);
  });
});

module.exports = router;
