'use strict';
const router = require('express').Router();
const user = require('./v1/user');
const time = require('./v1/time');

router.use('/user', user);
router.use('/time', time);

module.exports = router;
