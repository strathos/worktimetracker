'use strict';
const mongoose = require('mongoose');

module.exports = mongoose.model('appuser', new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: (true, "Username field is required")
  }
}));
